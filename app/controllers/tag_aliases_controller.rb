class TagAliasesController < ApplicationController
  respond_to :html

  def index
    @tag_aliases = TagAlias.where(tag_id: params[:tag_id])
    respond_with(@tag_aliases)
  end

  def show
        @tag_aliases = TagAlias.where(tag_id: params[:tag_id])

  end

  def new
    @tag_alias = TagAlias.new
  
  end

  def edit
  end

  def create
    @tag_alias = TagAlias.new(tag_alias_params)
    @tag_alias.save
    respond_with(@tag_alias.tag)
  end

  def update
    @tag_alias.update(tag_alias_params)
    respond_with(@tag_alias)
  end

  def destroy
    @tag_alias = TagAlias.where(tag_id: params[:tag_id], name: params[:name]).first
    @tag = @tag_alias.tag
    if @tag_alias.destroy
      redirect_to @tag
    end
  end

  private
    def set_tag_alias
      @tag_alias = TagAlias.find(params[:id])
    end

    def tag_alias_params
      params.require(:tag_alias).permit(:name, :tag_id)
    end
end
