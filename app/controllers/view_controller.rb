class ViewController < ApplicationController

	def index
		@view1 = Restaurant.find_by_sql ["SELECT DISTINCT restaurants.id, Restaurants.name 
										FROM ((restaurants JOIN restaurant_locations ON restaurants.id=restaurant_locations.restaurant_id) 
										JOIN Likes ON restaurants.id=Likes.restaurant_id) 
										WHERE (restaurant_locations.city = 'oshawa' AND Likes.user_id = 1)"]
	

		@view3 = Restaurant.find_by_sql ["SELECT DISTINCT
											restaurants.id, restaurants.name
											FROM
											restaurants , restaurant_locations 
											WHERE
											(restaurant_locations.city = 'oshawa' AND restaurants.id =
											restaurant_locations.restaurant_id)"]
=begin
This query executes without errors. However the object array returned doesn't hold the id/name attributes for some reason I couldn't figure. 
I think the problem is the INTERSECT.
You can try calling the array in views/view/index.html.erb
<%= @view5.to_s %>
<%= @view5.first.to_s %>	

	

		@view5 = Restaurant.find_by_sql ["SELECT
										restaurants.id, restaurants.name
										FROM
										restaurants JOIN likes ON
										restaurants.id=likes.restaurant_id
										WHERE likes.tag_id = 4 INTERSECT SELECT restaurants.id, restaurants.name FROM
										restaurants JOIN restaurant_locations ON
										restaurants.id = restaurant_locations.restaurant_id WHERE restaurant_locations.city = 'oshawa'
										"]
=end

=begin
This is an example of why view 6 is not possible in rails. Since we have to use 2 queries instead of 1 due to design limitations
		@view6A = Tag.find_by_sql ["SELECT
									tags.id, tags.name
									FROM
									tags  JOIN likes  ON tags.id = likes.tag_id
									GROUP BY
									tags.name"]
		@view6B = Tag.count_by_sql ["SELECT
									COUNT(likes.tag_id)
									FROM
									tags  JOIN likes  ON tags.id = likes.tag_id
									GROUP BY
									tags.name"]
=end
		@view7 = TagAlias.find_by_sql ["SELECT
										tag_aliases.name
										FROM
										tags  JOIN tag_aliases on tags.id =
										tag_aliases.tag_id
										WHERE
										tags.id = 4"]

	end
end
