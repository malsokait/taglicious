class ApplicationController < ActionController::Base
	include Twitter::Extractor
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :parser


	def parser
		 $client.search("to:tag_licious", result_type: "recent").each do |tweet| 
 		 	twt = tweet.text
 		 	twt_id = tweet.id
 		 	twt_uid = tweet.user.id.to_s
 		 	@user = User.find_by_uid(twt_uid)
 		 	@like = Like.find_by_id(twt_id)
 		 	if @like.nil?
	 		 	extract_hashtags(twt).each do |hashtag|
	 		 		@restaurant = Restaurant.where('lower(name) = ?', hashtag.humanize.downcase).first
	 		 		unless @restaurant
	 		 			@restaurant_alias = RestaurantAlias.where('lower(name) = ?', hashtag.humanize.downcase).first
	 		 			@restaurant = @restaurant_alias.restaurant unless @restaurant_alias.nil?
	 		 		end
	 		 		if @restaurant
	 		 			extract_hashtags(twt).each do |hashtag|
	 		 				@tag = Tag.where('lower(name) = ?', hashtag.humanize.downcase).first
	 		 				unless @tag
	 		 					@tag_alias = TagAlias.where('lower(name) = ?', hashtag.humanize.downcase).first
	 		 					@tag = @tag_alias.tag unless @tag_alias.nil?
	 		 				end
	 		 				if @tag
	 		 					unless @user.nil?
		 		 					like = Like.new(id: twt_id, restaurant_id: @restaurant.id, tag_id: @tag.id, user_id: @user.id) 
		 		 					like.save!
	 		 					end
	 		 				end
	 		 			end
	 		 		end
	 		 	end
 		 	end
        end 
	end
end
