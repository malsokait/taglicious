class RestaurantAliasesController < ApplicationController

  respond_to :html

  def index
    @restaurant_aliases = RestaurantAlias.where(restaurant_id: params[:restaurant_id])
    respond_with(@restaurant_aliases)
  end

  def show
    @restaurant_aliases = RestaurantAlias.where(restaurant_id: params[:restaurant_id])

  end

  def new
    @restaurant_alias = RestaurantAlias.new
    respond_with(@restaurant_alias)
  end

  def edit
  end

  def create
    @restaurant_alias = RestaurantAlias.new(restaurant_alias_params)
    @restaurant_alias.save
    respond_with(@restaurant_alias.restaurant)
  end

  def update
    @restaurant_alias.update(restaurant_alias_params)
    respond_with(@restaurant_alias)
  end

  def destroy
    @restaurant_alias = RestaurantAlias.where(restaurant_id: params[:restaurant_id], name: params[:name]).first
    @restaurant = @restaurant_alias.restaurant
    if @restaurant_alias.destroy
     redirect_to @restaurant
    end
  end

  private
    def set_restaurant_alias
      @restaurant_alias = RestaurantAlias.find(params[:id])
    end

    def restaurant_alias_params
      params.require(:restaurant_alias).permit(:name, :restaurant_id)
    end
end
