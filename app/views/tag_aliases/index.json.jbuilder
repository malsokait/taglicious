json.array!(@tag_aliases) do |tag_alias|
  json.extract! tag_alias, :id, :name, :tag_id
  json.url tag_alias_url(tag_alias, format: :json)
end
