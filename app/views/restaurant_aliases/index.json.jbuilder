json.array!(@restaurant_aliases) do |restaurant_alias|
  json.extract! restaurant_alias, :id, :name, :restaurant_id
  json.url restaurant_alias_url(restaurant_alias, format: :json)
end
