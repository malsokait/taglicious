json.array!(@restaurant_locations) do |restaurant_location|
  json.extract! restaurant_location, :id, :address, :phone, :lat, :lng, :city, :restaurant_id
  json.url restaurant_location_url(restaurant_location, format: :json)
end
