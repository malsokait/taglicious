class RestaurantLocation < ActiveRecord::Base
  belongs_to :restaurant

  validates :restaurant, presence: true
  validates :city, presence: true
end
