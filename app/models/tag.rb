class Tag < ActiveRecord::Base
	has_many :likes, dependent: :destroy
	has_many :tag_aliases, dependent: :destroy
	has_many :restaurants, through: :likes

	validates :name, presence: true, uniqueness: { case_sensitive: false}
end
