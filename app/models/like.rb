class Like < ActiveRecord::Base
  belongs_to :user
  belongs_to :tag
  belongs_to :restaurant
  validates :user, :tag, :restaurant, presence: true
end
