class TagAlias < ActiveRecord::Base
  belongs_to :tag
  self.primary_key = 'name'

  validates :tag, presence: true
  validates :name, presence: true, uniqueness: { case_sensitive: false}
end
