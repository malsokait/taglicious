class Restaurant < ActiveRecord::Base
	has_many :likes, dependent: :destroy
	has_many :restaurant_locations, dependent: :destroy
	has_many :restaurant_aliases, dependent: :destroy
	has_many :tags, through: :likes

	validates :name, presence: true, uniqueness: { case_sensitive: false}
end
