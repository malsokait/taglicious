class RestaurantAlias < ActiveRecord::Base
  belongs_to :restaurant
  self.primary_key = 'name'

  validates :restaurant, presence: true
  validates :name, presence: true, uniqueness: { case_sensitive: false}
end
