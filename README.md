README:

* App deployed at https://rocky-forest-6835.herokuapp.com/
* or you can clone the repository


* install rails: http://railsinstaller.org/
* clone repository: https://bitbucket.org/malsokait/taglicious
* go to taglicious folder on your console.
* run:
	bundle install
	rake db:migrate
	rails server
* go to http://127.0.0.1:3000

# USAGE # 
* You have to log in using Twitter once on the website or your tweets wont be parsed
* Tweet to @tag_licious #tag #restaurant
* or use #tag_alias #restaurant_alias



# VIEWS # 
### view 1: ###
List all restaurants in a city “Oshawa” with a like from user with user_id “5”
 /app/controllers/view_controller.rb -- /app/views/view/index.html.erb 

### view 3: ### 
List all restaurants in a specific city
In this example, city is “oshawa”
/app/controllers/view_controller.rb -- /app/views/view/index.html.erb 

### View 4: ###
Lists all restaurants with their locations. Restaurants without a location will be displayed as well.
/app/controllers/restaurant_controller.rb -- /app/views/restaurants/show.html.erb 

### view 5: ### 
Lists all restaurants that carry an item with tag_id and have a location in a city.
Here, tag_id is “4” and city is “Oshawa”
/app/controllers/view_controller.rb -- /app/views/view/index.html.erb 

### view 6: ### 
Lists all tagged items with the number of likes for each tag.
/app/controllers/tag_controller.rb -- /app/views/tags/index.html.erb 

### view 7: ###
Lists all aliases for a certain tag with tag_id
Here, tag_id is “4”
 /app/controllers/view_controller.rb -- /app/views/view/index.html.erb 

### view 8: ### 
Lists the number of likes for each tag in each restaurant.
/app/controllers/restaurant_controller.rb -- /app/views/restaurants/show.html.erb 

### view 9: ### 
Lists the number of likes for each tag.
/app/controllers/tag_controller.rb -- /app/views/tags/show.html.erb 

### view 10: ### 
Lists the number of likes for each restaurant.
/app/controllers/restaurant_controller.rb -- /app/views/restaurants/show.html.erb

### Tweets parser ###
/app/controllers/application_controller.rb