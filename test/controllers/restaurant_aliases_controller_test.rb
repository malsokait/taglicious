require 'test_helper'

class RestaurantAliasesControllerTest < ActionController::TestCase
  setup do
    @restaurant_alias = restaurant_aliases(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:restaurant_aliases)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create restaurant_alias" do
    assert_difference('RestaurantAlias.count') do
      post :create, restaurant_alias: { name: @restaurant_alias.name, restaurant_id: @restaurant_alias.restaurant_id }
    end

    assert_redirected_to restaurant_alias_path(assigns(:restaurant_alias))
  end

  test "should show restaurant_alias" do
    get :show, id: @restaurant_alias
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @restaurant_alias
    assert_response :success
  end

  test "should update restaurant_alias" do
    patch :update, id: @restaurant_alias, restaurant_alias: { name: @restaurant_alias.name, restaurant_id: @restaurant_alias.restaurant_id }
    assert_redirected_to restaurant_alias_path(assigns(:restaurant_alias))
  end

  test "should destroy restaurant_alias" do
    assert_difference('RestaurantAlias.count', -1) do
      delete :destroy, id: @restaurant_alias
    end

    assert_redirected_to restaurant_aliases_path
  end
end
