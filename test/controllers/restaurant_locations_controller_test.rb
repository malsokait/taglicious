require 'test_helper'

class RestaurantLocationsControllerTest < ActionController::TestCase
  setup do
    @restaurant_location = restaurant_locations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:restaurant_locations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create restaurant_location" do
    assert_difference('RestaurantLocation.count') do
      post :create, restaurant_location: { address: @restaurant_location.address, city: @restaurant_location.city, lat: @restaurant_location.lat, lng: @restaurant_location.lng, phone: @restaurant_location.phone, restaurant_id: @restaurant_location.restaurant_id }
    end

    assert_redirected_to restaurant_location_path(assigns(:restaurant_location))
  end

  test "should show restaurant_location" do
    get :show, id: @restaurant_location
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @restaurant_location
    assert_response :success
  end

  test "should update restaurant_location" do
    patch :update, id: @restaurant_location, restaurant_location: { address: @restaurant_location.address, city: @restaurant_location.city, lat: @restaurant_location.lat, lng: @restaurant_location.lng, phone: @restaurant_location.phone, restaurant_id: @restaurant_location.restaurant_id }
    assert_redirected_to restaurant_location_path(assigns(:restaurant_location))
  end

  test "should destroy restaurant_location" do
    assert_difference('RestaurantLocation.count', -1) do
      delete :destroy, id: @restaurant_location
    end

    assert_redirected_to restaurant_locations_path
  end
end
