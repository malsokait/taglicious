require 'test_helper'

class TagAliasesControllerTest < ActionController::TestCase
  setup do
    @tag_alias = tag_aliases(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tag_aliases)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tag_alias" do
    assert_difference('TagAlias.count') do
      post :create, tag_alias: { name: @tag_alias.name, tag_id: @tag_alias.tag_id }
    end

    assert_redirected_to tag_alias_path(assigns(:tag_alias))
  end

  test "should show tag_alias" do
    get :show, id: @tag_alias
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tag_alias
    assert_response :success
  end

  test "should update tag_alias" do
    patch :update, id: @tag_alias, tag_alias: { name: @tag_alias.name, tag_id: @tag_alias.tag_id }
    assert_redirected_to tag_alias_path(assigns(:tag_alias))
  end

  test "should destroy tag_alias" do
    assert_difference('TagAlias.count', -1) do
      delete :destroy, id: @tag_alias
    end

    assert_redirected_to tag_aliases_path
  end
end
