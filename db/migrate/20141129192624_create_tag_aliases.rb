class CreateTagAliases < ActiveRecord::Migration
  def change
    create_table :tag_aliases, :id => false do |t|
      t.string :name
      t.references :tag, index: true

      t.timestamps
    end
  end
end
