class CreateRestaurantLocations < ActiveRecord::Migration
  def change
    create_table :restaurant_locations do |t|
      t.string :address
      t.string :phone
      t.float :lat
      t.float :lng
      t.string :city
      t.references :restaurant, index: true

      t.timestamps
    end
  end
end
