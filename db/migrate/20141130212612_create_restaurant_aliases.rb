class CreateRestaurantAliases < ActiveRecord::Migration
  def change
    create_table :restaurant_aliases, :id => false do |t|
      t.string :name
      t.references :restaurant, index: true

    end
  end
end
