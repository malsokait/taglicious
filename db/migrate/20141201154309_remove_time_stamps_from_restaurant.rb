class RemoveTimeStampsFromRestaurant < ActiveRecord::Migration
  def change
  	remove_column :restaurants, :created_at
  	remove_column :restaurants, :updated_at
  end
end
