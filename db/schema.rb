# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141202003229) do

  create_table "likes", force: true do |t|
    t.integer  "user_id"
    t.integer  "tag_id"
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "likes", ["restaurant_id"], name: "index_likes_on_restaurant_id"
  add_index "likes", ["tag_id"], name: "index_likes_on_tag_id"
  add_index "likes", ["user_id"], name: "index_likes_on_user_id"

  create_table "restaurant_aliases", id: false, force: true do |t|
    t.string  "name"
    t.integer "restaurant_id"
  end

  add_index "restaurant_aliases", ["restaurant_id"], name: "index_restaurant_aliases_on_restaurant_id"

  create_table "restaurant_locations", force: true do |t|
    t.string   "address"
    t.string   "phone"
    t.float    "lat"
    t.float    "lng"
    t.string   "city"
    t.integer  "restaurant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "restaurant_locations", ["restaurant_id"], name: "index_restaurant_locations_on_restaurant_id"

  create_table "restaurants", force: true do |t|
    t.string "name"
  end

  create_table "tag_aliases", id: false, force: true do |t|
    t.string   "name"
    t.integer  "tag_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tag_aliases", ["tag_id"], name: "index_tag_aliases_on_tag_id"

  create_table "tags", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "twitter_handle"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "provider"
    t.string   "uid"
    t.string   "password"
    t.string   "name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
